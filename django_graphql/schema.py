import graphene
from graphene import ObjectType

import api.schema
from api.graphql.follow.create.mutation import FollowCreate
from api.graphql.follow.update.mutation import FollowUpdate
from api.graphql.idea.create.mutation import IdeaCreate
from api.graphql.idea.delete.mutation import IdeaDelete
from api.graphql.idea.update_visibility.mutation import IdeaUpdateVisibility
from api.graphql.user.change_password.mutation import ChangePassword
from api.graphql.user.create.mutation import UserCreate


class Query(api.schema.Query, graphene.ObjectType):
    pass


class Mutation(ObjectType):
    user_created = UserCreate.Field()
    change_password = ChangePassword.Field()
    idea_create = IdeaCreate.Field()
    idea_delete = IdeaDelete.Field()
    idea_update_visibility = IdeaUpdateVisibility.Field()
    follow_create = FollowCreate.Field()
    follow_update = FollowUpdate.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
