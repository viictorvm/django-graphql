from django.contrib import admin
from django.urls import path
from graphene_django.views import GraphQLView
from rest_framework.authtoken.views import obtain_auth_token

from api.decorators import token_required

urlpatterns = [
    path('admin/', admin.site.urls),
    path('graphql', token_required(GraphQLView.as_view(graphiql=True))),
    path('api-token-auth/', obtain_auth_token, name='api_token_auth'),
]
