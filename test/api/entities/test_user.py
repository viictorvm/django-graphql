from unittest import TestCase

from api.entities.user import User
from api.exceptions import InvalidEmailFormat, InvalidPasswordFormat


class TestUser(TestCase):

    @classmethod
    def setUp(cls):
        cls.default_password = 'example1234'
        cls.default_username = 'username1234'
        cls.default_email = 'email@email.es'

    def test_create_user_with_wrong_email_format_should_return_invalid_email_format_exception(self):
        with self.assertRaises(InvalidEmailFormat):
            User(password=self.default_password, username=self.default_username, email='test')

    def test_create_user_with_wrong_password_format_should_return_invalid_password_format_exception(self):
        with self.assertRaises(InvalidPasswordFormat):
            User(password='a', username=self.default_username, email=self.default_email)

    def test_create_user_should_not_return_any_exception(self):
        user = User(password=self.default_username, username=self.default_username, email=self.default_email)
        self.assertEqual(user.email, self.default_email)
        self.assertEqual(user.password, self.default_password)
        self.assertEqual(user.username, self.default_username)
