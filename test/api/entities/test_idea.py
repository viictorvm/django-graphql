from unittest import TestCase

from api.entities.idea import Idea
from api.entities.user import User
from api.entities.visibility import Visibility
from api.exceptions import InvalidTextIdeaFormat


class TestIdea(TestCase):

    @classmethod
    def setUp(cls):
        cls.default_user = User(username='example', email='example@e.es', password="examplePassword")
        cls.default_visibility = Visibility.private

    def test_create_idea_with_none_text_return_invalid_text_idea_format_exception(self):
        with self.assertRaises(InvalidTextIdeaFormat):
            Idea(user=self.default_user, visibility=self.default_visibility, text=None)

    def test_create_idea_with_empty_text_return_invalid_text_idea_format_exception(self):
        with self.assertRaises(InvalidTextIdeaFormat):
            Idea(user=self.default_user, visibility=self.default_visibility, text=[])

    def test_create_idea_with_text_greater_than_limit_return_invalid_text_idea_format_exception(self):
        text = "x" * (Idea.TEXT_MAX_LEN + 1)
        with self.assertRaises(InvalidTextIdeaFormat):
            Idea(user=self.default_user, visibility=self.default_visibility, text=text)
