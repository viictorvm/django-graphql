from unittest import TestCase

from api.service.encode_password_service import EncodePasswordService


class TestEncodePasswordService(TestCase):

    @classmethod
    def setUp(cls):
        cls.encode_password_service = EncodePasswordService()

    def test_encode_two_strings_case_sensitive_should_return_same_value(self):
        encoded_1 = self.encode_password_service.exec('Example')
        encoded_2 = self.encode_password_service.exec('Example')

        self.assertEqual(encoded_1, encoded_2)

    def test_encode_two_strings_non_case_sensitive_should_no_return_same_value(self):
        encoded_1 = self.encode_password_service.exec('Example')
        encoded_2 = self.encode_password_service.exec('example')

        self.assertNotEqual(encoded_1, encoded_2)
