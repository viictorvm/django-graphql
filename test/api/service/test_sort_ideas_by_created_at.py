from datetime import date, timedelta
from unittest import TestCase
from unittest.mock import MagicMock

from api.service.sort_ideas_list_by_created_at import SortIdeasListByCreatedAt


class TestSortIdeasByCreatedAt(TestCase):

    @classmethod
    def setUp(cls):
        cls.today_date = date.today()

    def test_order_empty_list_should_return_empty(self):
        sorted_ideas = SortIdeasListByCreatedAt.sort([])
        self.assertEqual(sorted_ideas, [])

    def test_order_two_ideas_should_return_ideas_list_with_correct_order_by_created_at(self):
        yesterday_date = self.today_date - timedelta(days=1)
        idea_1 = MagicMock(text='Example 1 oldest', created_at=yesterday_date)
        idea_2 = MagicMock(text='Example 2 newest', created_at=self.today_date)

        sorted_ideas = SortIdeasListByCreatedAt.sort([idea_1, idea_2])
        self.assertEqual(sorted_ideas[0].text, idea_2.text)
        self.assertEqual(sorted_ideas[1].text, idea_1.text)

    def test_order_two_ideas_with_same_datetime_should_return_ideas_listin_same_order_by_created_at(self):
        idea_1 = MagicMock(text='Example 1 same datetime', created_at=self.today_date)
        idea_2 = MagicMock(text='Example 2 same datetime', created_at=self.today_date)

        sorted_ideas = SortIdeasListByCreatedAt.sort([idea_1, idea_2])
        self.assertEqual(sorted_ideas[0].text, idea_1.text)
        self.assertEqual(sorted_ideas[1].text, idea_2.text)
