import graphene
from graphene_django.types import DjangoObjectType

from api.exceptions import InvalidArgument
from api.handler.find_idea_by_username import FindIdeaByUsernameHandler
from api.models import Follow, Idea, User


class UserType(DjangoObjectType):
    class Meta:
        model = User


class IdeaType(DjangoObjectType):
    class Meta:
        model = Idea


class FollowType(DjangoObjectType):
    class Meta:
        model = Follow


class Query(graphene.AbstractType):
    all_users = graphene.List(UserType)
    idea_by_username = graphene.List(IdeaType, user=graphene.String())
    all_ideas = graphene.List(IdeaType)
    all_follows = graphene.List(FollowType)

    def resolve_all_users(self, context, **kwargs):
        return User.objects.all()

    def resolve_all_ideas(self, context, **kwargs):
        return Idea.objects.all()

    def resolve_all_follows(self, context, **kwargs):
        return Follow.objects.all()

    def resolve_idea_by_username(self, context, **kwargs):
        username = kwargs.get('user', None)
        if not username:
            raise InvalidArgument
        return FindIdeaByUsernameHandler(username=username).handle()
