from api.exceptions import InvalidArgument
from api.graphql.user.change_password.input import ChangePasswordInputDTO


class ChangePasswordInputObjectFromInput:

    @staticmethod
    def transform(input):
        username = input.get('username', None)
        password = input.get('password', None)

        if username is None or password is None:
            raise InvalidArgument

        return ChangePasswordInputDTO(username=username, password=password)
