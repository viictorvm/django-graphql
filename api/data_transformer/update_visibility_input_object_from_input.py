from api.data_transformer.create_visibility_enum_from_string import CreateVisibilityEnumFromString
from api.graphql.idea.update_visibility.input import IdeaUpdateVisibilityInput


class UpdateVisibilityInputObjectFromInput:

    @staticmethod
    def transform(input):
        idea_id = input.get('idea_id', None)
        visibility = input.get('visibility', None)
        visibility_enum = CreateVisibilityEnumFromString.transform(visibility)

        return IdeaUpdateVisibilityInput(idea_id=idea_id, visibility=visibility_enum)
