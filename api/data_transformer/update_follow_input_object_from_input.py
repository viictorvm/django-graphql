from api.data_transformer.create_follow_status_enum_from_string import CreateFollowStatusEnumFromString
from api.exceptions import InvalidArgument
from api.graphql.follow.update.input import UpdateFollowInput


class UpdateFollowInputObjectFromInput:

    @staticmethod
    def transform(input):
        follow_to = input.get('follow_to', None)
        follow_from = input.get('follow_from', None)
        status = input.get('status', None)

        if follow_to is None or follow_from is None or status is None:
            raise InvalidArgument

        status = CreateFollowStatusEnumFromString.transform(status)
        return UpdateFollowInput(follow_to=follow_to, follow_from=follow_from, status=status)
