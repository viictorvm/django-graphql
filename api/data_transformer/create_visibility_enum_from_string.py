from api.entities.visibility import Visibility


class CreateVisibilityEnumFromString:

    @staticmethod
    def transform(string_visibility):
        for data in Visibility:
            if data.name == string_visibility:
                return data
        return Visibility.private
