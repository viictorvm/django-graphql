from api.exceptions import InvalidArgument
from api.graphql.user.create.input import CreateUserInput


class CreateUserInputObjectFromInput:

    @staticmethod
    def transform(input):
        username = input.get('username', None)
        email = input.get('email', None)
        password = input.get('password', None)

        if username is None or email is None or password is None:
            raise InvalidArgument

        return CreateUserInput(username=username, email=email, password=password)
