from api.entities.follow_status import FollowStatus
from api.exceptions import InvalidArgument


class CreateFollowStatusEnumFromString:

    @staticmethod
    def transform(string_status):
        for data in FollowStatus:
            if data.name == string_status:
                return data

        raise InvalidArgument
