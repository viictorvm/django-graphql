from api.exceptions import InvalidArgument
from api.graphql.idea.create.input import CreateIdeaInput


class CreateIdeaInputObjectFromInput:

    @staticmethod
    def transform(input):
        visibility = input.get('visibility', None)
        text = input.get('text', None)
        username = input.get('username', None)

        if username is None or username is None:
            raise InvalidArgument

        return CreateIdeaInput(text=text, visibility=visibility, username=username)
