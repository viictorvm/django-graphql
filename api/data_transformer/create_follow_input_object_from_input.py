from api.exceptions import InvalidArgument
from api.graphql.follow.create.input import CreateFollowInput


class CreateFollowInputObjectFromInput:

    @staticmethod
    def transform(input):
        follow_to = input.get('follow_to', None)
        follow_from = input.get('follow_from', None)

        if follow_to is None or follow_from is None:
            raise InvalidArgument

        return CreateFollowInput(follow_to=follow_to, follow_from=follow_from)
