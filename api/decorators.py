from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token

from api.exceptions import InvalidToken


def token_required(func):
    @csrf_exempt
    def inner(request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION', None)
        if token is None:
            raise InvalidToken
        parsed_token = token.replace('Bearer ', '')
        token_queried = Token.objects.filter(key=parsed_token).first()
        if token_queried is None:
            raise InvalidToken

        return func(request, *args, **kwargs)

    return inner
