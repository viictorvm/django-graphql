from api.exceptions import InvalidArgument
from api.graphql.idea.update_visibility.input import IdeaUpdateVisibilityInput
from api.handler.base_handler import BaseHandler
from api.service.update_idea_visibility import UpdateIdeaVisibilityService


class UpdateIdeaVisibilityHandler(BaseHandler):

    def __init__(self, update_idea_visibility: IdeaUpdateVisibilityInput):
        self.update_idea_visibility_service = UpdateIdeaVisibilityService()
        self.update_idea_visibility = update_idea_visibility

    def handle(self):
        if not self.update_idea_visibility.idea_id or not self.update_idea_visibility.visibility:
            raise InvalidArgument
        return self.update_idea_visibility_service.exec(update_idea_visibility=self.update_idea_visibility)
