from api.exceptions import InvalidArgument
from api.graphql.follow.update.input import UpdateFollowInput
from api.handler.base_handler import BaseHandler
from api.service.finder.find_follow_service_by_username import FindFollowByUsernameService
from api.service.next_follow_status_by_current import NextFollowStatusByCurrentService
from api.service.update_follow_service import UpdateFollowService


class UpdateFollowHandler(BaseHandler):

    def __init__(self, follow_input: UpdateFollowInput):
        self.follow_input = follow_input
        self.find_follow_service = FindFollowByUsernameService()
        self.update_follow_service = UpdateFollowService()
        self.next_follow_status_service = NextFollowStatusByCurrentService()

    def handle(self):
        follow = self.find_follow_service.exec(follow_to=self.follow_input.follow_to,
                                               follow_from=self.follow_input.follow_from)
        if follow is None:
            raise InvalidArgument

        status_options = self.next_follow_status_service.exec(follow.status)
        if self.follow_input.status not in status_options:
            raise InvalidArgument

        return self.update_follow_service.exec(id=follow.id, status=self.follow_input.status)
