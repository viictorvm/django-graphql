from api.entities.follow import Follow
from api.entities.follow_status import FollowStatus
from api.exceptions import UsernameNotFound, InvalidArgument
from api.graphql.follow.create.input import CreateFollowInput
from api.handler.base_handler import BaseHandler
from api.service.find_user_by_username_service import FindUserByUsernameService
from api.service.persist_follow_service import PersistFollowService


class CreateFollowHandler(BaseHandler):

    def __init__(self, follow_input: CreateFollowInput):
        self.follow_input = follow_input
        self.find_user_by_username_service = FindUserByUsernameService()
        self.persist_follow_service = PersistFollowService()

    def handle(self):
        if self.follow_input.follow_from == self.follow_input.follow_to:
            raise InvalidArgument

        follow_to = self.find_user_by_username_service.exec(self.follow_input.follow_to)
        follow_from = self.find_user_by_username_service.exec(self.follow_input.follow_from)
        if follow_to is None or follow_from is None:
            raise UsernameNotFound

        follow = Follow(follow_from=follow_from, follow_to=follow_to, status=FollowStatus.requested)
        return self.persist_follow_service.exec(follow=follow)
