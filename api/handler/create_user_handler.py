from api.entities.user import User
from api.exceptions import UsernameRepeated
from api.graphql.user.create.input import CreateUserInput
from api.handler.base_handler import BaseHandler
from api.service.find_user_by_username_service import FindUserByUsernameService
from api.service.persist_user_service import PersistUserService


class CreateUserHandler(BaseHandler):

    def __init__(self, user_input: CreateUserInput):
        self.user_input = user_input
        self.find_user_by_username_service = FindUserByUsernameService()
        self.persist_user_service = PersistUserService()

    def handle(self):
        user = self.find_user_by_username_service.exec(self.user_input.username)
        if user is not None:
            raise UsernameRepeated

        user = User(username=self.user_input.username, email=self.user_input.email, password=self.user_input.password)
        return self.persist_user_service.exec(user=user)


