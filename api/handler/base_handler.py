from abc import abstractmethod, ABC


class BaseHandler(ABC):

    @abstractmethod
    def handle(self):
        pass
