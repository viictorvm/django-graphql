from api.handler.base_handler import BaseHandler
from api.service.finder.find_ideas_by_username import FindIdeasByUsernameService
from api.service.sort_ideas_list_by_created_at import SortIdeasListByCreatedAt


class FindIdeaByUsernameHandler(BaseHandler):
    def __init__(self, username):
        self.username = username
        self.find_ideas_by_username = FindIdeasByUsernameService()
        self.sort_ideas_list_by_created_at = SortIdeasListByCreatedAt()

    def handle(self):
        ideas = self.find_ideas_by_username.exec(self.username)
        return self.sort_ideas_list_by_created_at.sort(ideas_list=ideas)
