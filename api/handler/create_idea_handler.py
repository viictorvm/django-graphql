from api.data_transformer.create_visibility_enum_from_string import CreateVisibilityEnumFromString
from api.entities.idea import Idea
from api.exceptions import UsernameNotFound
from api.graphql.idea.create.input import CreateIdeaInput
from api.handler.base_handler import BaseHandler
from api.service.find_user_by_username_service import FindUserByUsernameService
from api.service.persist_idea_service import PersistIdeaService


class CreateIdeaHandler(BaseHandler):

    def __init__(self, idea_input: CreateIdeaInput):
        self.idea_input = idea_input
        self.find_user_by_username_service = FindUserByUsernameService()
        self.create_visibility_enum = CreateVisibilityEnumFromString()
        self.persist_idea_service = PersistIdeaService()

    def handle(self):
        user = self.find_user_by_username_service.exec(self.idea_input.username)
        if user is None:
            raise UsernameNotFound

        visibility = self.create_visibility_enum.transform(self.idea_input.visibility)
        idea = Idea(user=user, text=self.idea_input.text, visibility=visibility)

        return self.persist_idea_service.exec(idea=idea)
