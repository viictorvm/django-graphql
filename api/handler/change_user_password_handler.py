from api.exceptions import InvalidArgument
from api.graphql.user.change_password.input import ChangePasswordInputDTO
from api.handler.base_handler import BaseHandler
from api.service.change_user_password_service import ChangeUserPasswordService
from api.service.encode_password_service import EncodePasswordService
from api.service.find_user_by_username_service import FindUserByUsernameService


class ChangeUserPasswordHandler(BaseHandler):

    def __init__(self, change_password_input: ChangePasswordInputDTO):
        self.change_password_input = change_password_input
        self.find_user_by_username_service = FindUserByUsernameService()
        self.change_user_password_service = ChangeUserPasswordService()
        self.encode_password_service = EncodePasswordService()

    def handle(self):
        user = self.find_user_by_username_service.exec(self.change_password_input.username)
        if user is None:
            raise InvalidArgument

        self.change_password_input.password = self.encode_password_service.exec(
            password=self.change_password_input.password)
        result = self.change_user_password_service.exec(change_password_input=self.change_password_input)
