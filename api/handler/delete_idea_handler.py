from api.graphql.idea.delete.input import DeleteIdeaInput
from api.handler.base_handler import BaseHandler
from api.service.delete_idea_service import DeleteIdeaService


class DeleteIdeaHandler(BaseHandler):

    def __init__(self, delete_idea_input: DeleteIdeaInput):
        self.delete_idea_service = DeleteIdeaService()
        self.delete_idea_input = delete_idea_input

    def handle(self):
        return self.delete_idea_service.exec(idea_id=self.delete_idea_input.idea_id)
