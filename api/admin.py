from django.contrib import admin

from .models import User, Idea, Follow

admin.site.register(User)
admin.site.register(Idea)
admin.site.register(Follow)
