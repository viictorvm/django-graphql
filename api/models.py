from datetime import datetime

from django.db import models


class User(models.Model):
    email = models.CharField(max_length=50, null=False, unique=True)
    username = models.CharField(max_length=20, null=False, unique=True)
    password = models.CharField(max_length=60, null=False)
    id = models.AutoField(primary_key=True)
    deleted = models.BooleanField(default=False)
    created_at = models.DateField(null=False, default=datetime.now())


class Idea(models.Model):
    text = models.CharField(max_length=100, null=False)
    visibility = models.CharField(max_length=15)
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    id = models.AutoField(primary_key=True)
    deleted = models.BooleanField(default=False)
    created_at = models.DateField(null=False, default=datetime.now())


class Follow(models.Model):
    user_source = models.ForeignKey(User, related_name="person_being_followed", on_delete=models.SET_NULL, null=True,
                                    blank=True)
    follower = models.ForeignKey(User, related_name="person_who_follows", on_delete=models.SET_NULL, null=True,
                                 blank=True)
    status = models.CharField(max_length=50, null=False)
    id = models.AutoField(primary_key=True, unique=True)
    deleted = models.BooleanField(default=False)
    created_at = models.DateField(null=False, default=datetime.now())
