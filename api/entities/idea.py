
from api.entities.visibility import Visibility
from api.exceptions import InvalidTextIdeaFormat


class Idea:
    TEXT_MAX_LEN = 100

    def __init__(self, user, text, visibility: Visibility):
        self.user = user
        self.text = self.__check_text(text)
        self.visibility = visibility

    def __check_text(self, text):
        if not text or len(text) > Idea.TEXT_MAX_LEN:
            raise InvalidTextIdeaFormat
        return text
