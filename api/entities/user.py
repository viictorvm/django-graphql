import re

from api.exceptions import InvalidEmailFormat, InvalidPasswordFormat


class User:
    # TODO move email regex logic to an email entitiy and split responsabilities.
    # same with password
    EMAIL_REGEX = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
    PASSWORD_MIN_LENGTH = 5

    def __init__(self, username, email, password):
        self.username = username
        self.email = self.__check_email(email)
        self.password = self.__check_password(password)

    def __check_email(self, email):
        if not re.search(self.EMAIL_REGEX, email):
            raise InvalidEmailFormat
        return email

    def __check_password(self, password):
        if not password or len(password) < self.PASSWORD_MIN_LENGTH:
            raise InvalidPasswordFormat
        return password
