from api.entities.follow_status import FollowStatus


class Follow:

    def __init__(self, follow_to, follow_from, status: FollowStatus):
        self.follow_to = follow_to
        self.follow_from = follow_from
        self.status = status