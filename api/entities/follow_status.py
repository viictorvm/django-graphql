from enum import Enum


class FollowStatus(Enum):
    requested = 1
    accepted = 2
    rejected = 3
