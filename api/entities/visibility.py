from enum import Enum


class Visibility(Enum):
    public = 1
    protected = 2
    private = 3
