from api.models import Idea


class IdeaRepository:

    def __init__(self):
        self.table = Idea

    def persist_idea(self, idea):
        idea_db = Idea(text=idea.text, visibility=idea.visibility.name, owner=idea.user)
        idea_db.save()
        return idea_db.id

    def find_idea_by_id(self, id):
        return self.table.objects.filter(id=id, deleted=False).first()

    def delete_idea_by_id(self, id):
        idea = self.find_idea_by_id(id=id)
        idea.deleted = True
        idea.save()

    def update_idea_visibility(self, id, visibility):
        idea = self.find_idea_by_id(id=id)
        idea.visibility = visibility.name
        idea.save()

    def find_idea_by_user(self, user):
        return list(Idea.objects.filter(owner=user))
