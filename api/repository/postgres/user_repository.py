from api.graphql.user.change_password.input import ChangePasswordInputDTO
from api.models import User


class UserRepository:

    def __init__(self):
        self.table = User

    def find_user_by_username(self, username):
        return self.table.objects.filter(username=username, deleted=False).first()

    def persist_user(self, user):
        user_db = User(username=user.username, email=user.email, password=user.password)
        user_db.save()
        return user_db.email

    def update_user_password_by_username(self, change_password_input: ChangePasswordInputDTO):
        username = self.find_user_by_username(username=change_password_input.username)
        username.password = change_password_input.password
        username.save()
