from api.models import Follow


class FollowRepository:

    def __init__(self):
        self.table = Follow

    def persist_follow(self, follow):
        follow_db = Follow(status=follow.status.name, user_source=follow.follow_from, follower=follow.follow_to)
        follow_db.save()
        return follow_db.id

    def find_follow(self, follow_from, follow_to):
        return self.find_follow_by_username(follow_from=follow_from.username, follow_to=follow_to.username)

    def find_follow_by_username(self, follow_from, follow_to):
        return Follow.objects.filter(user_source__username=follow_from, follower__username=follow_to,
                                     deleted=False).first()

    def update_follow_status_by_id(self, id, status):
        follow = Follow.objects.get(id=id)
        follow.status = status.name
        follow.save()
