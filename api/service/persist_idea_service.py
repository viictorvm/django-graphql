from api.entities.idea import Idea
from api.repository.postgres.idea_repository import IdeaRepository


class PersistIdeaService:

    def __init__(self):
        self.idea_repository = IdeaRepository()

    def exec(self, idea: Idea):
        return self.idea_repository.persist_idea(idea=idea)
