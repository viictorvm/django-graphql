from api.exceptions import IdeaNotFoundFormat
from api.graphql.idea.update_visibility.input import IdeaUpdateVisibilityInput
from api.repository.postgres.idea_repository import IdeaRepository


class UpdateIdeaVisibilityService:

    def __init__(self):
        self.idea_repository = IdeaRepository()

    def exec(self, update_idea_visibility: IdeaUpdateVisibilityInput):
        idea = self.idea_repository.find_idea_by_id(update_idea_visibility.idea_id)
        if not idea:
            raise IdeaNotFoundFormat
        return self.idea_repository.update_idea_visibility(id=update_idea_visibility.idea_id,
                                                           visibility=update_idea_visibility.visibility)
