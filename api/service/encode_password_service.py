import hashlib


class EncodePasswordService:

    def exec(self, password):
        password = password.encode('utf_8')
        return hashlib.sha224(password).hexdigest()
