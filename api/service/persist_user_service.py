from api.entities.user import User
from api.repository.postgres.user_repository import UserRepository
from api.service.encode_password_service import EncodePasswordService


class PersistUserService:

    def __init__(self):
        self.user_repository = UserRepository()
        self.encode_password_service = EncodePasswordService()

    def exec(self, user: User):
        user.password = self.encode_password_service.exec(user.password)
        return self.user_repository.persist_user(user=user)
