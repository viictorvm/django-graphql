from api.repository.postgres.user_repository import UserRepository


class FindUserByUsernameService:

    def __init__(self):
        self.user_repository = UserRepository()

    def exec(self, username):
        return self.user_repository.find_user_by_username(username)
