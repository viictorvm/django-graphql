from api.entities.follow import Follow
from api.exceptions import FollowRepeated
from api.repository.postgres.follow_repository import FollowRepository
from api.service.finder.find_follow_service import FindFollowService


class PersistFollowService:

    def __init__(self):
        self.follow_repository = FollowRepository()
        self.find_follow = FindFollowService()

    def exec(self, follow: Follow):
        follow_exist = self.follow_repository.find_follow(follow_from=follow.follow_from, follow_to=follow.follow_to)
        if follow_exist:
            raise FollowRepeated
        return self.follow_repository.persist_follow(follow=follow)
