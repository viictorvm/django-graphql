from api.exceptions import IdeaNotFoundFormat
from api.repository.postgres.idea_repository import IdeaRepository


class DeleteIdeaService:

    def __init__(self):
        self.idea_repository = IdeaRepository()

    def exec(self, idea_id):
        idea = self.idea_repository.find_idea_by_id(id=idea_id)
        if idea is None:
            raise IdeaNotFoundFormat
        return self.idea_repository.delete_idea_by_id(id=idea_id)
