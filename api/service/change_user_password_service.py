from api.graphql.user.change_password.input import ChangePasswordInputDTO
from api.repository.postgres.user_repository import UserRepository


class ChangeUserPasswordService:

    def __init__(self):
        self.user_repository = UserRepository()

    def exec(self, change_password_input: ChangePasswordInputDTO):
        return self.user_repository.update_user_password_by_username(change_password_input)
