from api.entities.follow_status import FollowStatus
from api.repository.postgres.follow_repository import FollowRepository


class UpdateFollowService:

    def __init__(self):
        self.follow_repository = FollowRepository()

    def exec(self, id, status: FollowStatus):
        return self.follow_repository.update_follow_status_by_id(id=id, status=status)
