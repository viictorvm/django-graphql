from api.entities.follow_status import FollowStatus


class NextFollowStatusByCurrentService:

    def exec(self, current_status):
        if current_status == FollowStatus.requested.name:
            return [FollowStatus.accepted, FollowStatus.rejected]
        return []
