from api.exceptions import UsernameNotFound
from api.repository.postgres.idea_repository import IdeaRepository
from api.repository.postgres.user_repository import UserRepository


class FindIdeasByUsernameService:

    def __init__(self):
        self.idea_repository = IdeaRepository()
        self.user_repository = UserRepository()

    def exec(self, username):
        user = self.user_repository.find_user_by_username(username=username)
        if user is None:
            raise UsernameNotFound
        return self.idea_repository.find_idea_by_user(user=user)
