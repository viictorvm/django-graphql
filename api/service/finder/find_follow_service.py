from api.repository.postgres.follow_repository import FollowRepository


class FindFollowService:

    def __init__(self):
        self.follow_repository = FollowRepository()

    def exec(self, follow_from, follow_to):
        return self.follow_repository.find_follow_by_username(follow_from=follow_from, follow_to=follow_to)
