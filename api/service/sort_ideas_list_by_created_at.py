class SortIdeasListByCreatedAt:

    @staticmethod
    def sort(ideas_list):
        if not len(ideas_list):
            return []
        ideas_list.sort(key=lambda idea: idea.created_at, reverse=True)
        return ideas_list
