from graphql import GraphQLError


class InvalidArgument(GraphQLError):
    def __init__(self):
        self.message = 'Invalid argument'
        super().__init__(message=self.message)


class UsernameRepeated(GraphQLError):
    def __init__(self):
        self.message = 'Username repeated'
        super().__init__(message=self.message)


class UsernameNotFound(GraphQLError):
    def __init__(self):
        self.message = 'Username not found'
        super().__init__(message=self.message)


class InvalidEmailFormat(GraphQLError):
    def __init__(self):
        self.message = 'Invalid email format'
        super().__init__(message=self.message)


class InvalidTextIdeaFormat(GraphQLError):
    def __init__(self):
        self.message = 'Invalid text idea format'
        super().__init__(message=self.message)


class InvalidPasswordFormat(GraphQLError):
    def __init__(self):
        self.message = 'Invalid password format'
        super().__init__(message=self.message)


class IdeaNotFoundFormat(GraphQLError):
    def __init__(self):
        self.message = 'Idea not found'
        super().__init__(message=self.message)
        super().__init__(message=self.message)


class InvalidToken(GraphQLError):
    def __init__(self):
        self.message = 'Invalid token'
        super().__init__(message=self.message)


class FollowRepeated(GraphQLError):
    def __init__(self):
        self.message = 'Follow repeated'
        super().__init__(message=self.message)
