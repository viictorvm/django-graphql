from graphene import InputObjectType, String


class CreateFollowInputGraphql(InputObjectType):
    follow_to = String()
    follow_from = String()


class CreateFollowInput:
    def __init__(self, follow_to, follow_from):
        self.follow_to = follow_to
        self.follow_from = follow_from
