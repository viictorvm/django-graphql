from graphene import Boolean, Mutation, String

from api.data_transformer.create_follow_input_object_from_input import CreateFollowInputObjectFromInput
from api.exceptions import InvalidArgument
from api.graphql.follow.create.input import CreateFollowInputGraphql
from api.handler.create_follow_handler import CreateFollowHandler


class FollowCreate(Mutation):
    output = Boolean()
    idea_inserted = String()

    class Arguments:
        input = CreateFollowInputGraphql()

    @classmethod
    def mutate(cls, root, info, **data):
        input = data.get('input', None)
        if input is None:
            return InvalidArgument
        idea_input = CreateFollowInputObjectFromInput.transform(input)
        result = CreateFollowHandler(idea_input).handle()

        return FollowCreate(output=True, idea_inserted=result)
