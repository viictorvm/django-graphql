import graphene
from graphene import Boolean


class Response:
    status = Boolean()
