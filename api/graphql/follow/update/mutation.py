from graphene import Boolean, Mutation, String

from api.data_transformer.update_follow_input_object_from_input import UpdateFollowInputObjectFromInput
from api.exceptions import InvalidArgument
from api.graphql.follow.update.input import UpdateFollowInputGraphql
from api.handler.update_follow_handler import UpdateFollowHandler


class FollowUpdate(Mutation):
    output = Boolean()
    idea_inserted = String()

    class Arguments:
        input = UpdateFollowInputGraphql()

    @classmethod
    def mutate(cls, root, info, **data):
        input = data.get('input', None)
        if input is None:
            return InvalidArgument
        idea_input = UpdateFollowInputObjectFromInput.transform(input)
        result = UpdateFollowHandler(idea_input).handle()

        return FollowUpdate(output=True, idea_inserted=result)
