from graphene import InputObjectType, String

from api.entities.follow_status import FollowStatus


class UpdateFollowInputGraphql(InputObjectType):
    follow_to = String()
    follow_from = String()
    status = String()


class UpdateFollowInput:
    def __init__(self, follow_to, follow_from, status: FollowStatus):
        self.follow_to = follow_to
        self.follow_from = follow_from
        self.status = status
