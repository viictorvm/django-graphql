from graphene import Boolean, Mutation, String

from api.data_transformer.update_visibility_input_object_from_input import UpdateVisibilityInputObjectFromInput
from api.exceptions import InvalidArgument
from api.graphql.idea.update_visibility.input import IdeaUpdateVisibilityInputGraphql
from api.handler.update_idea_visibility_handler import UpdateIdeaVisibilityHandler


class IdeaUpdateVisibility(Mutation):
    output = Boolean()
    idea_inserted = String()

    class Arguments:
        input = IdeaUpdateVisibilityInputGraphql()

    @classmethod
    def mutate(cls, root, info, **data):
        input = data.get('input', None)
        if input is None:
            return InvalidArgument
        idea_input = UpdateVisibilityInputObjectFromInput.transform(input)
        result = UpdateIdeaVisibilityHandler(idea_input).handle()

        return IdeaUpdateVisibility(output=True, idea_inserted=result)
