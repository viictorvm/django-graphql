from graphene import InputObjectType, String


class IdeaUpdateVisibilityInputGraphql(InputObjectType):
    idea_id = String()
    visibility = String()


class IdeaUpdateVisibilityInput:
    def __init__(self, idea_id, visibility):
        self.idea_id = idea_id
        self.visibility = visibility
