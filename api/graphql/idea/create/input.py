from graphene import InputObjectType, String


class CreateIdeaInputGraphql(InputObjectType):
    text = String()
    visibility = String()
    username = String()


class CreateIdeaInput:
    def __init__(self, text, visibility, username):
        self.text = text
        self.visibility = visibility
        self.username = username
