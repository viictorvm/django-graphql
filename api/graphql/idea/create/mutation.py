from graphene import Boolean, Mutation, String

from api.data_transformer.create_idea_input_object_from_input import CreateIdeaInputObjectFromInput
from api.exceptions import InvalidArgument
from api.graphql.idea.create.input import CreateIdeaInputGraphql
from api.handler.create_idea_handler import CreateIdeaHandler


class IdeaCreate(Mutation):
    output = Boolean()
    idea_inserted = String()

    class Arguments:
        input = CreateIdeaInputGraphql()

    @classmethod
    def mutate(cls, root, info, **data):
        input = data.get('input', None)
        if input is None:
            return InvalidArgument
        idea_input = CreateIdeaInputObjectFromInput.transform(input)
        result = CreateIdeaHandler(idea_input).handle()

        return IdeaCreate(output=True, idea_inserted=result)
