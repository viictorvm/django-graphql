from graphene import InputObjectType, String


class DeleteIdeaInputGraphql(InputObjectType):
    idea_id = String()


class DeleteIdeaInput:
    def __init__(self, idea_id):
        self.idea_id = idea_id
