from graphene import Boolean, Mutation
from api.exceptions import InvalidArgument
from api.graphql.idea.delete.input import DeleteIdeaInputGraphql, DeleteIdeaInput
from api.handler.delete_idea_handler import DeleteIdeaHandler


class IdeaDelete(Mutation):
    output = Boolean()

    class Arguments:
        input = DeleteIdeaInputGraphql()

    @classmethod
    def mutate(cls, root, info, **data):
        input = data.get('input', None)
        if input is None:
            return InvalidArgument
        idea_id = input.get('idea_id', None)
        delete_idea_input = DeleteIdeaInput(idea_id=idea_id)
        DeleteIdeaHandler(delete_idea_input).handle()

        return IdeaDelete(output=True)
