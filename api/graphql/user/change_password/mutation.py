from graphene import Boolean, Mutation, String

from api.data_transformer.change_password_input_object_from_input import ChangePasswordInputObjectFromInput
from api.exceptions import InvalidArgument
from api.graphql.user.change_password.input import ChangePasswordInput
from api.handler.change_user_password_handler import ChangeUserPasswordHandler


class ChangePassword(Mutation):
    output = Boolean()
    email_updated = String()

    class Arguments:
        input = ChangePasswordInput(required=True)

    @classmethod
    def mutate(cls, root, info, **data):
        input = data.get('input', None)
        if input is None:
            return InvalidArgument
        user_input = ChangePasswordInputObjectFromInput.transform(input)
        result = ChangeUserPasswordHandler(user_input).handle()

        return ChangePassword(output=True, email_updated=result)
