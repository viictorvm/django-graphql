from graphene import InputObjectType, String


class ChangePasswordInput(InputObjectType):
    username = String()
    password = String()


class ChangePasswordInputDTO:
    def __init__(self, username, password):
        self.username = username
        self.password = password
