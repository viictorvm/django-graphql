from graphene import Boolean, InputObjectType, Mutation, String

from api.data_transformer.create_user_input_object_from_input import CreateUserInputObjectFromInput
from api.exceptions import InvalidArgument
from api.graphql.user.create.input import CreateUserInput, CreateUserInputGraphql

from api.graphql.user.create.response import Response
from api.handler.create_user_handler import CreateUserHandler


class UserCreate(Mutation):
    output = Boolean()
    email_inserted = String()

    class Arguments:
        input = CreateUserInputGraphql()

    @classmethod
    def mutate(cls, root, info, **data):
        input = data.get('input', None)
        if input is None:
            return InvalidArgument
        user_input = CreateUserInputObjectFromInput.transform(input)
        result = CreateUserHandler(user_input).handle()

        return UserCreate(output=True, email_inserted=result)
