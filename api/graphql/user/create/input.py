from graphene import InputObjectType, String


#
class CreateUserInputGraphql(InputObjectType):
    username = String()
    email = String()
    password = String()


class CreateUserInput:
    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = password
