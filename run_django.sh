#!/bin/sh
python3 manage.py makemigrations api
python3 manage.py migrate --noinput
python3 manage.py shell -c "exec(\"from django.contrib.auth.models import User\\nif not User.objects.filter(username='admin'):\\n    User.objects.create_superuser('admin', 'admin', 'admin')\")"
python manage.py runserver 0.0.0.0:8000
