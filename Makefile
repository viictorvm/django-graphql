.PHONY: start
start: stop build up

.PHONY: stop
stop:
		docker-compose stop

.PHONY: build
build:
		docker-compose build && \
		docker-compose pull

.PHONY: up
up:
		docker-compose up -d

.PHONY: exec
exec:
		python manage.py runserver

.PHONY: migrations
migrations:
		python manage.py makemigrations api

.PHONY: migrate
migrate:
		python manage.py migrate api
