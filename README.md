App development using [Django](https://www.djangoproject.com/) and [GraphQL](https://graphql.org/). We decided to follow a [DDD](https://justdigital.agency/que-es-domain-driven-design-ddd/)
pattern, in order to build scalable code and split our app into different layers.

* On one hand we have the repository folder, where we have the responsibility to connect our backend with our DB, 
in this case postgreSQL DB. In case we want to move to MongoDb, for example, we just have to implement those files and our app won’t know it.
* On the other hand, we have the handlers folder, which receives all of the request from each graphql request.
* In the services folder we have every business rules.
* Entities represent our internal models but which are not related with DB. 
* Models, How to represent data in our DB. We are using django ORM, but in this case it's used as repository, 
to split the business logic and our database model.
* GraphQL, here we have everything to set up correctly our graphql API. It's splitted just because otherwise our API will depend on
Graphql, and this will give us the possibility to use other API like REST, or mix them.

# Decisions
1. Each request will be parsed into a GraphQLInput, but each handler will receive
any custom DTO in terms to separate Django, Grahpql and the business logic. For example, if we want to use 
REST API, or have a mix.

2. For auth against our system, we will use Django rest framework auth using tokens. Graphql has a nice auth method,
but the goal of the whole project is to split dependencies and don't depend on graphql. As a improvement, I would change
Django auth for Auth0.

3. If the visibility of any idea is incorrect, the default visibility would be private.
4. In our DB model, we have two "extra" fields; created_at and deleted. The reason behind this is to follow logical 
deletion.

5. One admin user is created by default.

6. To use just our API, we will need a valid token, which belongs to each user of OUR system (not the normal users). The 
reason of this, is to separate permissions, so in this case, any frontend (mobile, react... etc) will keep this token and will
use it with all the users. As default, we will need to create one token for Admin user, and then we will be able to
make requests.

7. Each request will be handled by a HandlerClass, which include N ServicesClass, and those services have
the communication of each repository. As the rest, is the same goal to separate responsibilities in term if we 
need to escalate our app in the future or maybe we need to change any part of our system as clean as possible.

# Containers.
+ Django image
+ PostgreSQL
+ Nginx (just if we want to deploy it in the cloud, we will need it).


# Endpoints
+ `api-token-auth/` Create token for each user of the system given username and password
+ `admin/` Django admin page Database.
+ `graphql`

For example to create one user on /graphql:
```graphql
mutation {
  userCreated(
    input:
    {
      username: "usernameExample",
      email: "example@eexample.es"
      password: "passwordExample"
    }
  ) {
      output
    }
}
```
# Improvements
+ Integration tests
+ Include Auth0 
+ Graphql responses, make it separately from graphql.
+ Handle raised exceptions.
+ Script for django container to wait until postgres container is up.
